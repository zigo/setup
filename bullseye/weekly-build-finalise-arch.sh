#!/bin/bash

ARCH=$1
FIRMWARE=$2

# Pull in the core settings for all builds
. settings.sh
. weekly.sh
. common.sh

# Pull together all the build output, sign, publish. etc.

if [ "$FIRMWARE"x != ""x ]; then
    PUBDIRJIG="${PUBDIRJIG}-firmware"
    RSYNC_TARGET="$RSYNC_TARGET_FIRMWARE"
    WITH_F=" with firmware"
    FIRMWARE="firmware" # used below for the call to iso_run
fi

if [ "$DRYRUN"x != ""x ]; then
    echo "Dry-run $0: ARCH $ARCH$WITH_F"
    exit 0
fi

echo "$ARCH$WITH_F: all individual builds complete, finalising..."

# Grab errors and the logs for this arch/firmware combo
for TRACE in $PUBDIRJIG/$ARCH/*.trace; do
    BUILDNAME=$(basename $TRACE ".trace")
    case $BUILDNAME in
        *firmware*)
	    BUILD_INC_FIRMWARE=1;;
        *)
	    BUILD_INC_FIRMWARE=0;;
    esac
    if ([ "$FIRMWARE"x = ""x ] && [ $BUILD_INC_FIRMWARE = 0 ]) ||
       ([ "$FIRMWARE"x != ""x ] && [ $BUILD_INC_FIRMWARE = 1 ]); then
	. $TRACE
	if [ $error -ne 0 ]; then
	    arch_error="$arch_error "$BUILDNAME"FAIL/$error/$end/$logfile"
	fi
        cp log/$logfile ${PUBDIRJIG}/$ARCH/$BUILDNAME.log
    fi
done

# If we had any errors for this arch, report them. Do *not* do
# anything else - leave the last successful build in place for users
if [ "$arch_error"x != ""x ] ; then
    echo "  Reporting that $ARCH$WITH_F failed, arch_error $arch_error"
    ~/build.${CODENAME}/report_build_error ${PUBDIRJIG} \
	    $RSYNC_TARGET $ARCH "$arch_error"
    exit 0
fi

# No errors, so continue onwards here
if [ "$RELEASE_BUILD"x = ""x ] ; then
    echo "  $ARCH$WITH_F: Not generating torrent files for a non-release build"
else
    echo "  $ARCH$WITH_F: Generating torrent files"
    ~/build.${CODENAME}/mktorrent ${PUBDIRJIG}/$ARCH/iso-*/*.iso
fi

echo "  $ARCH$WITH_F: Generating checksum files"
for dir in ${PUBDIRJIG}/$ARCH/jigdo-*; do
    generate_checksums_for_arch $ARCH $dir
done

if [ "$RELEASE_BUILD"x = ""x ] ; then
    echo "  $ARCH$WITH_F: Signing checksums files using the automatic key"
    ~/build.${CODENAME}/sign-images ${PUBDIRJIG} $ARCH
fi

if [ "$NOSYNC"x = ""x ] ; then
    echo "  $ARCH$WITH_F: running iso_run ${PUBDIRJIG}/ $RSYNC_TARGET/ $ARCH"
    if [ "$FIRMWARE"x = ""x ]; then
	IRL=~/build.${CODENAME}/log/$CODENAME-$ARCH.iso_run
    else
	IRL=~/build.${CODENAME}/log/$CODENAME-$ARCH-firmware.iso_run
    fi
    START=$(now)
    ~/build.${CODENAME}/iso_run ${PUBDIRJIG}/ \
	    $RSYNC_TARGET/ $ARCH "$FIRMWARE" > $IRL 2>&1
    END=$(now)
    SPENT=$(calc_time $START $END)
    echo "  $ARCH$WITH_F: iso_run finished, took $SPENT"
fi
